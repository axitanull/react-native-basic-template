### SETUP Dependencies

- nodejs v8.x
- Android SDK (You may need to import ./android to Android Studio and run it once to check which SDK dependencies are needed)
- XCode >= 8.x

```
npm install
gulp compile-template -e=dev  # Compile templates from build-resources
```

# Development

To build or run project in debugging mode, you must at least open/run the project with Android Studio/XCode

To start React Native Debugging Server (for code hot reloading), run:

```npm run start```

Then open Android Studio/XCode, and run project with simulator

# Run unit test

```npm test```

# Run Project on IOS Simulator

Open XCode -> Open xcodeproj -> Run

or

```npm run ios```

## Run Project on Android Simulator

Open Android Studio -> Import from existing Gradle Project -> Run

or

```npm run android```

## Building apk/ipa

Generated with ```gulp --list```

```
Available tasks
  build-android     Build android APK
  build-ios         Create iOS Archive
  compile-template  Compile files under templates/master with given environment, options: -e (environment)
  help              Display this help text.
  setup             setup for given environment, options: -e (environment)
```
