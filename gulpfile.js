const gulp = require('gulp-help')(require('gulp'));
const template = require('gulp-template');
const runSequence = require('run-sequence');
const argv = require('yargs').argv;
const chalk = require('chalk');
const spawn = require('child_process').spawn;

/**
 *Basic parameters
 */
const ENVIRONMENT = (argv.env || argv.e || 'dev').toLowerCase();

/**
 * Print this in bright color!
 */
console.log(chalk.yellow.underline.bold(`Doing task for environment: ${ENVIRONMENT}`));

gulp.task("setup", "setup for given environment, options: -e (environment)", function (cb) {
    runSequence("compile-template", cb);
});

gulp.task("build-android", "Build android APK", function (cb) {
    let args = [`assembleRelease`];

    //- Execute something like: $ ./gradlew assembleRelease
    let buildProcess = spawn('./gradlew', args, {cwd: 'android'});
    buildProcess.on('error', function (err) {
        throw err;
    });
    buildProcess.on('close', function () {
        console.log('Result apk resides in : android/app/build/outputs/apk');
        cb();
    });
    buildProcess.stdout.pipe(process.stdout);
    buildProcess.stderr.pipe(process.stderr);
});

gulp.task("build-ios", "Create iOS Archive", function (cb) {
    let name = "reactnativebasictemplate"; //TODO: change this to project name
    let args = ['archive', '-scheme', name, '-archivePath', name, '-sdk', 'iphoneos', '-configuration', 'Release'];
    let buildProcess = spawn('xcodebuild', args, {cwd: 'ios'});
    buildProcess.on('error', function (err) {
        throw err;
    });
    buildProcess.on('close', function () {
        console.log(`Result archive resides in : ios/${name}.xcarchive`);
        cb();
    });
    buildProcess.stdout.pipe(process.stdout);
    buildProcess.stderr.pipe(process.stderr);
});

gulp.task("compile-template", "Compile files under templates/master with given environment, options: -e (environment)", function (cb) {
    let environmentStringValues = require(`./build-resources/template/env/${ENVIRONMENT}/string.js`);
    let constantStringValues = require('./build-resources/template/env/constant.js');

    gulp.src('build-resources/template/master/**/*', {base: 'build-resources/template/master/'})
        .pipe(template(Object.assign(environmentStringValues, constantStringValues), {
            interpolate: /<%=([\s\S]+?)%>/g //Ignore ES6 syntax ${} replacement
        }))
        .pipe(gulp.dest('.'))
        .on('end', function () {
            cb();
        })
});
