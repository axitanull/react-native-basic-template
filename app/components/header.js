import React, {Component} from "react";
import {View, Text} from "react-native";
import PropTypes from "prop-types";

class Header extends Component {
    static propTypes = {
        environment: PropTypes.string,
    };

    static defaultProps = {
        environment: "Unknown",
    };

    render() {
        return (
            <View style={{width: "100%", height: 50, backgroundColor: "#aaa"}}>
                <Text>{this.props.environment}</Text>
                <Text>component inside app/components/header.js</Text>
            </View>
        )
    }
}

export default Header;